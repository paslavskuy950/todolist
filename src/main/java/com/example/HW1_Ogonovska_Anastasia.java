package com.example;

import java.util.Scanner;

public class HW1_Ogonovska_Anastasia {

    public static void main(String[] args) {
        double a1 = 10;
        double b1 = 5;
        double c1 = 11;
        double d1 = 6;
        double result;

        result = a1 + b1 * (c1 - d1) / 2;

        System.out.println(result);

        double a;
        double b;
        double c;

        System.out.println("Квадратне рівння");
        System.out.println("ax^2 + bx + c = 0");
        System.out.println("Введіть a, b, c");

        Scanner in = new Scanner(System.in);

        a = in.nextDouble();
        b = in.nextDouble();
        c = in.nextDouble();

        double D = b * b - 4 * a * c;

        if (D > 0) {
            double x1, x2;
            x1 = (-b - Math.sqrt(D)) / (2 * a);
            x2 = (-b + Math.sqrt(D)) / (2 * a);
            System.out.println(x1);
            System.out.println(x2);
        } else if (D == 0) {
            double x;
            x = -b / (2 * a);
            System.out.println(x);
        } else {
            System.out.println("Коренів немає");
        }

    }
}
